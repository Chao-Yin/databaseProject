create table user_information
(
    uuid     varchar(255) primary key not null comment '用户信息表对应职工id',
    password varchar(16)     not null comment '用户对应的密码'
);