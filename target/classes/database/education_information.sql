create table education_information
(
    id                    int          not null primary key auto_increment comment '学历信息主键',
    userId                int          not null comment '学历信息对应用户id',
    education_name        varchar(255) null comment '学历名称',
    education_major       varchar(255) null comment '专业',
    education_get_year    date         null comment '学历获得年份',
    education_institution varchar(255) null comment '学历机构',
    foreign_language      varchar(255) null comment '外语情况',
    extra_key1            varchar(255) null comment '预留字段1',
    extra_key2            varchar(255) null comment '预留字段2',
    extra_key3            int          null comment '预留字段3'
) charset utf8;