create table employee_basic_information
(
    uuid            int primary key null comment '职工信息表主键',
    nationality     varchar(255)    null comment '国籍',
    sex             char(2)         null comment '性别',
    birthday        date            null comment '生日',
    age             int             null comment '年龄',
    marriage_status varchar(255)    null comment '婚姻状态',
    name            varchar(255)    null comment '姓名',
    extra_key2      varchar(255)    null comment '预留字段2',
    extra_key3      int             null comment '预留字段3'
) charset utf8;
