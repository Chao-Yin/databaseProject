create table employee_job_information
(
    id         int primary key not null auto_increment comment '员工岗位信息表',
    uuid       int             not null comment '员工工号',
    job_id     int             not null comment '岗位id',
    start_time date            null comment '岗位开始信息',
    end_time   date            null comment '岗位结束时间',
    job_status varchar(255) comment '岗位',
    extra_key1 varchar(255)    null comment '预留字段1',
    extra_key2 varchar(255)    null comment '预留字段2',
    extra_key3 int             null comment '预留字段3'
) charset utf8;