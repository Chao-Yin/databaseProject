import constant.Constant;
import org.jdbi.v3.core.Jdbi;
import org.junit.Test;

/**
 * @author yinchao
 * @Date 2019/12/9 17:47
 */
public class TestClass {

    @Test
    public void testMethod(){
        Jdbi jdbi = Jdbi.create(Constant.URL,Constant.USER_NAME,Constant.USER_PASSWORD);
        jdbi.useHandle(handle -> handle.execute("insert into user_information values (123,123);"));
    }
}
