package constant;

import java.awt.*;

/**
 * @author yinchao
 * @Date 2019/12/9 17:48
 */
public class Constant {
    public static final String URL = "jdbc:mysql://47.100.76.82:3306/database_homework?useSSL=false&characterEncoding=UTF-8";

    public static final String USER_NAME = "guest";

    public static final String USER_PASSWORD = "guest@yinchao";

    public static final Color COLOR1 = new Color(0xF6B352);
    public static final Color COLOR2 = new Color(0xF68657);
    public static final Color COLOR3 = new Color(0x383A3F);
    public static final Color COLOR4 = new Color(0x1F2124);
    public static final Color COLOR5 = new Color(0x379392);
    public static final Color COLOR6 = new Color(0x4FB0C6);
    public static final Color COLOR7 = new Color(0x4F86C6);
    public static final Color COLOR8 = new Color(0x6C49B8);
    public static final String SQL = "select user_information.uuid, name, sex, " +
            "age, birthday, marriage_status, " +
            "nationality, education_name, education_get_year, education_major ," +
            "foreign_language , education_institution, employee_job_information.start_time, " +
            "department_name, department.job, employee_job_information.job_status " +
            "from education_information, " +
            "user_information, employee_basic_information, employee_job_information, department " +
            "where employee_basic_information.uuid = user_information.uuid " +
            "and employee_basic_information.uuid = education_information.userId " +
            "and employee_basic_information.uuid = employee_job_information.uuid " +
            "and employee_job_information.job_id = department.id " +
            "and education_get_year = ( " +
            "select max(education_get_year) " +
            "from education_information " +
            "where education_information.userId = employee_basic_information.uuid) " +
            "and start_time = ( " +
            "select max(employee_job_information.start_time) " +
            "from employee_job_information " +
            "where employee_job_information.uuid = user_information.uuid)";
    public static final String BASIC_SQL = "select user_information.uuid, name, sex, " +
            "age, birthday, marriage_status, " +
            "nationality, education_name, education_get_year, education_major ," +
            "foreign_language , education_institution, employee_job_information.start_time, " +
            "department_name, department.job, employee_job_information.job_status " +
            "from education_information, " +
            "user_information, employee_basic_information, employee_job_information, department " +
            "where employee_basic_information.uuid = user_information.uuid " +
            "and employee_basic_information.uuid = education_information.userId " +
            "and employee_basic_information.uuid = employee_job_information.uuid " +
            "and employee_job_information.job_id = department.id ";
    public static String[] HEADERS = new String[]{"工号", "姓名", "性别", "年龄", "生日", "国籍", "婚姻状态",
            "学历名称", "专业", "学位获得年份", "学位获得院校", "外语情况",
            "工作职位", "所在部门", "工作状态", "岗位开始时间"};

    public static Font newFONTBold() {
        return new Font("宋体", Font.BOLD, 20);
    }

    public static Font newFontPlain() {
        return new Font("宋体", Font.PLAIN, 20);
    }
}
