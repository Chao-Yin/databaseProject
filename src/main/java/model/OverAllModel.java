package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OverAllModel {
    private String uuid;
    private String name;
    private String sex;
    private int age;
    private String birthday;
    private String nationality;
    private String marriageStatus;
    private String job;
    private String departmentName;
    private String jobStatus;
    private String educationName;
    private String educationMajor;
    private String educationGetYear;
    private String educationInstitution;
    private String foreignLanguage;
}
