package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeJobInformationModel {
    private int id;
    private String uuid;
    private int jobId;
    private String startTime;
    private String endTime;
    private String jobStatus;
    private String extraKey1;
    private String extraKey2;
    private int extraKey3;
}
