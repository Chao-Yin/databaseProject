package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentModel {
    private int id;
    private String departmentName;
    private String job;
    private String extraKey1;
    private String extraKey2;
    private int extraKey3;
}
