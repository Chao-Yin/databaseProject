package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EducationInformationModel {
    private int id;
    private String userId;
    private String educationName;
    private String educationMajor;
    private String educationGetYear;
    private String educationInstitution;
    private String foreignLanguage;
    private String extraKey1;
    private String extraKey2;
    private int extraKey3;
}
