package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeBasicInformationModel {
    private String uuid;
    private String nationality;
    private String sex;
    private String birthday;
    private int age;
    private String marriageStatus;
    private String name;
    private String extraKey2;
    private int extraKey3;
}
