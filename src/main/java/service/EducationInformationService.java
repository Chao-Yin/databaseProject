package service;

import constant.Constant;
import lombok.extern.slf4j.Slf4j;
import model.EducationInformationModel;
import org.jdbi.v3.core.Jdbi;

import java.time.LocalDate;
import java.util.List;

@Slf4j(topic = "EducationInformationService")
public class EducationInformationService {
    Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public EducationInformationModel selectByPrimaryKey(String userId) {
        EducationInformationModel educationInformationModel = jdbi.withHandle(handle -> handle
                .createQuery("select * from education_information as table1 where userId = :userId and education_get_year = (select max(education_get_year) from education_information as table2 where table1.userId = table2.userId )")
                .bind("userId", userId)
                .mapToBean(EducationInformationModel.class)
                .one());
        log.info(educationInformationModel.toString());
        return educationInformationModel;
    }

    public Integer updateOrInsert(EducationInformationModel educationInformationModel) {
        System.out.println(educationInformationModel);
        if (selectByPrimaryKey(educationInformationModel.getUserId()) != null) {
            return jdbi.withHandle(handle -> handle.createUpdate("update education_information set education_name=:educationName,education_major = :educationMajor,education_get_year= :educationGetYear, education_institution= :educationInstitution, foreign_language = :foreignLanguage where id = :id")
                    .bindBean(educationInformationModel)
                    .execute());
        } else {
//            return insert(educationInformationModel);
            return -1;
        }
    }

    private Integer insert(EducationInformationModel educationInformationModel) {
        return jdbi.withHandle(handle -> handle.createUpdate("insert into education_information(userId,education_mame,educatiion_major,education_get_year,education_institution,foreign_language) values (:userId, :educationName, :educationMajor,:educationGetYear, :educationInstitution, :foreignLanguage)")
                .bindBean(educationInformationModel)
                .execute());
    }

    public List<EducationInformationModel> selectAll() {
        return jdbi.withHandle(handle -> handle.createQuery("select * from education_information")
                .mapToBean(EducationInformationModel.class)
                .list());
    }

    public void create(String uuid) {
        LocalDate localDate = LocalDate.now();
        jdbi.useHandle(handle -> handle.createUpdate("insert into education_information(userId,education_name,education_major,education_get_year,education_institution,foreign_language) values (:uuid, null, null,:now, null, null)")
                .bind("uuid", uuid)
                .bind("now", localDate)
                .execute());
    }
}
