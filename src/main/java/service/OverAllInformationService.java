package service;

import constant.Constant;
import lombok.extern.slf4j.Slf4j;
import model.OverAllModel;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

@Slf4j(topic = "OverAllInformationService")
public class OverAllInformationService {
    Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public List<OverAllModel> selectAllNewestJobStatusAndHighestEducation() {
        return jdbi.withHandle(handle -> handle
                .createQuery(Constant.SQL)
                .mapToBean(OverAllModel.class).list());
    }

    public List<OverAllModel> selectAllNewestJobStatusAndHighestEducationByMarriage(String marriage) {
        log.info(Constant.SQL + " and marriage_status = '" + marriage + "'");
        return jdbi.withHandle(handle -> handle.
                createQuery(Constant.SQL + "\n and marriage_status = :marriage")
                .bind("marriage", marriage)
                .mapToBean(OverAllModel.class)
                .list());
    }

    public List<OverAllModel> selectAllNewestJobStatusAndHighestEducationByEducation(String education) {
        return jdbi.withHandle(handle -> handle.createQuery(Constant.BASIC_SQL + " and education_name = :education_name"
                + " and start_time = (select max(start_time) from employee_job_information where employee_job_information.uuid = user_information.uuid)")
                .bind("education_name", education)
                .mapToBean(OverAllModel.class)
                .list());
    }

    public List<OverAllModel> selectAllNewestJobStatusAndHighestEducationByJob(String job) {
        return jdbi.withHandle(handle -> handle
                .createQuery(Constant.BASIC_SQL + " and education_get_year = (select max(education_get_year) from education_information where education_information.userId = user_information.uuid)"
                        + " and job = :job")
                .bind("job", job)
                .mapToBean(OverAllModel.class)
                .list());
    }
}
