package service;

import constant.Constant;
import org.jdbi.v3.core.Jdbi;

import java.util.Optional;

public class DepartmentService {
    Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public Optional<Integer> jobId(String job) {
        System.out.println(job);
        //Integer i= jdbi.withHandle(handle -> handle.execute("select id from department where job = 'job'"));
        //return i;
        return jdbi.withHandle(handle -> handle
                .createQuery("select id from department where job = :job")
                .bind("job", job)
                .mapTo(Integer.class)
                .findOne());
    }

    public Optional<String> departmentName(int jobId) {
        return jdbi.withHandle(handle -> handle
                .createQuery("select job from department where id = :jobId")
                .bind("jobId", jobId)
                .mapTo(String.class)
                .findOne());
    }
}
