package service;

import constant.Constant;
import lombok.extern.slf4j.Slf4j;
import model.EmployeeJobInformationModel;
import org.jdbi.v3.core.Jdbi;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j(topic = "JobInformationService")
public class JobInformationService {
    Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public Optional<EmployeeJobInformationModel> selectByPrimaryKey(String uuid) {
        Optional<EmployeeJobInformationModel> employeeJobInformationModel = jdbi.withHandle(handle -> handle
                .createQuery("select * from employee_job_information as table1 where uuid = :uuid and start_time = (select max(start_time) from employee_job_information as table2 where table2.uuid = table1.uuid)")
                .bind("uuid", uuid)
                .mapToBean(EmployeeJobInformationModel.class)
                .findOne());
        log.info(employeeJobInformationModel.toString());
        return employeeJobInformationModel;
    }

    public Integer updateOrInsert(EmployeeJobInformationModel employeeJobInformationModel) {
        System.out.println(employeeJobInformationModel);
        if (selectByPrimaryKey(employeeJobInformationModel.getUuid()).isPresent()) {
            return jdbi.withHandle(handle -> handle.createUpdate("update employee_job_information set job_id=:jobId,job_status=:jobStatus,start_time=:startTime ,end_time= :endTime  where id = :id")
                    .bindBean(employeeJobInformationModel)
                    .execute());
        } else {
            return -1;
            // insert(employeeJobInformationModel);
        }
    }

    public void create(String uuid) {
        jdbi.useHandle(handle -> handle.createUpdate("insert into employee_job_information(uuid,job_id,start_time,end_time,job_status) values (:uuid, 1, :start,null,null)")
                .bind("uuid",uuid)
                .bind("start", LocalDate.now())
                .execute());
    }

    private Integer insert(EmployeeJobInformationModel employeeJobInformationModel) {
        return jdbi.withHandle(handle -> handle.createUpdate("insert into employee_job_information(uuid,job_status,start_time,end_time) values (:uuid,: status, :Job_StartTime, :Job_EndTime,)")
                .bindBean(employeeJobInformationModel)
                .execute());
    }

    public List<EmployeeJobInformationModel> selectAll() {
        return jdbi.withHandle(handle -> handle.createQuery("select * from employee_job_information")
                .mapToBean(EmployeeJobInformationModel.class)
                .list());
    }
}
