package service;

import constant.Constant;
import model.EmployeeBasicInformationModel;
import org.jdbi.v3.core.Jdbi;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class EmployeeBasicInformationService {
    Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public Optional<EmployeeBasicInformationModel> selectByPrimaryKey(String uuid) {
        return jdbi.withHandle(handle -> handle
                .createQuery("select * from employee_basic_information where uuid = :uuid")
                .bind("uuid", uuid)
                .mapToBean(EmployeeBasicInformationModel.class)
                .findOne());
    }

    public void create(String uuid)
    {
        jdbi.useHandle(handle -> handle.createUpdate("insert into employee_basic_information(uuid,birthday) values (:uuid,:birthday)")
                .bind("uuid",uuid)
                .bind("birthday", LocalDate.now())
                .execute());
    }

    public Integer updateOrInsert(EmployeeBasicInformationModel employeeBasicInformationModel) {
        System.out.println(employeeBasicInformationModel);
        if (selectByPrimaryKey(employeeBasicInformationModel.getUuid()).isPresent()) {
            return jdbi.withHandle(handle -> handle.createUpdate("update employee_basic_information set nationality=:nationality,sex = :sex,birthday = :birthday, age = :age, marriage_status = :marriageStatus, name = :name where uuid = :uuid")
                    .bindBean(employeeBasicInformationModel)
                    .execute());
        } else {
            return insert(employeeBasicInformationModel);
        }
    }

    private Integer insert(EmployeeBasicInformationModel employeeBasicInformationModel) {
        return jdbi.withHandle(handle -> handle.createUpdate("insert into employee_basic_information(uuid,nationality,sex,birthday,age,marriage_status,name) values (:uuid, :nationality, :sex,:birthday, :age, :marriageStatus,:name)")
                .bindBean(employeeBasicInformationModel)
                .execute());
    }

    public List<EmployeeBasicInformationModel> selectAll() {
        return jdbi.withHandle(handle -> handle.createQuery("select * from employee_basic_information")
                .mapToBean(EmployeeBasicInformationModel.class)
                .list());
    }
}
