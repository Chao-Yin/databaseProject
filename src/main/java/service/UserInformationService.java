package service;

import constant.Constant;
import lombok.extern.slf4j.Slf4j;
import model.UserInformationModel;
import org.jdbi.v3.core.Jdbi;

import java.util.Optional;

@Slf4j(topic = "userInformationService")
public class UserInformationService {
    private Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);

    public Optional<UserInformationModel> selectByPrimaryKey(String uuid) {
        return jdbi.withHandle(handle -> handle.createQuery("select * from user_information where uuid = :uuid")
                .bind("uuid", uuid)
                .mapToBean(UserInformationModel.class)
                .findOne());
    }

    public void insert(String uuid, String password) {
        jdbi.useHandle(handle -> handle.createUpdate("insert into user_information(uuid,password) values (:uuid, :password)")
                .bind("uuid", uuid).bind("password", password).execute());
    }

    public void updateByPrimaryKey(String uuid, String password) {
        jdbi.useHandle(handle -> {
            handle.createUpdate("update user_information set password = :password where uuid = :uuid")
                    .bind("uuid", uuid)
                    .bind("password", password).execute();
        });
    }

    public Integer deleteByPrimaryKey(String uuid) {
        return jdbi.withHandle(handle -> handle.createUpdate("delete from user_information where uuid = :uuid")
                .bind("uuid", uuid)
                .execute());
    }
}
