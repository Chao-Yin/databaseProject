import constant.Constant;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

/**
 * @author yinchao
 * @Date 2019/12/9 16:24
 */
public class Main {
    public static void main(String[] args) {
        String sql = "select uuid from user_information";
        Jdbi jdbi = Jdbi.create(Constant.URL, Constant.USER_NAME, Constant.USER_PASSWORD);
        List<String> stringList = jdbi.withHandle(handle -> {
            return handle.createQuery(sql)
                    .mapTo(String.class)
                    .list();
        });
        stringList.forEach(System.out::println);
    }
}
