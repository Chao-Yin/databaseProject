package gui;

import constant.Constant;
import model.EmployeeBasicInformationModel;
import service.EmployeeBasicInformationService;
import util.Util;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class UserInformationFrame extends JFrame {
    private static UserInformationFrame singleton;
    /*员工详细信息填写
    包含出生日期，婚姻状况，学历，专业，毕业时间，学校，工作岗位，工作部门
     */
    //三张表 基本信息表 学历信息表 工作信息表
    //学历信息表：学历 毕业时间 大学 专业 是否过英语六级
    //工作信息表 工作岗位 曾工作岗位 曾工作岗位开始时间 曾工作岗位结束时间
    //基本信息表
    private JTextField birthday;
    private JTextField marriageStatus;
    private JTextField nationality;
    private JTextField age;
    private JTextField sex;
    private JTextField name;
    //行为
    private EmployeeBasicInformationService employeeBasicInformationService = new EmployeeBasicInformationService();

    private UserInformationFrame() {
    }

    UserInformationFrame(String uuid) {
        Optional<EmployeeBasicInformationModel> employeeBasicInformationModel =
                employeeBasicInformationService.selectByPrimaryKey(uuid);
        if (!employeeBasicInformationModel.isPresent()) {
            employeeBasicInformationModel = Optional.of(new EmployeeBasicInformationModel());
            employeeBasicInformationModel.get().setUuid(uuid);
            //id=uuid;
        }

        // 创建一个容器
        Container container = this.getContentPane();
        JLabel jLabelUUID = new JLabel("UUID:" + employeeBasicInformationModel.get().getUuid());
        jLabelUUID.setBounds(350, 5, 200, 50);
        jLabelUUID.setFont(Constant.newFONTBold());

        name = new JTextField(employeeBasicInformationModel.get().getName());
        name.setBounds(450, 50, 200, 30);
        name.setFont(Constant.newFONTBold());
        name.setText(employeeBasicInformationModel.get().getName());
        JLabel jLabelName = new JLabel("姓名");
        jLabelName.setBounds(250, 50, 50, 30);
        jLabelName.setFont(Constant.newFontPlain());
        // 性别信息输入框
        sex = new JTextField(String.valueOf(employeeBasicInformationModel.get().getSex()));
        sex.setBounds(450, 100, 200, 30);
        sex.setFont(Constant.newFONTBold());
        JLabel jLabelSex = new JLabel("性别:");
        jLabelSex.setBounds(250, 100, 50, 30);
        jLabelSex.setFont(Constant.newFontPlain());

        // 年龄信息输入框
        age = new JTextField(String.valueOf(employeeBasicInformationModel.get().getAge()));
        age.setBounds(450, 150, 200, 30);
        age.setFont(Constant.newFONTBold());
        JLabel jLabelAge = new JLabel("年龄");
        jLabelAge.setBounds(250, 150, 50, 30);
        jLabelAge.setFont(Constant.newFontPlain());

        // 出生日期输入框
        birthday = new JTextField(employeeBasicInformationModel.get().getBirthday());
        birthday.setBounds(450, 200, 200, 30);
        birthday.setFont(Constant.newFONTBold());
        JLabel jLabelBirthday = new JLabel("出生日期：");
        jLabelBirthday.setBounds(250, 200, 150, 40);
        jLabelBirthday.setFont(Constant.newFontPlain());

        // 国籍信息输入框
        nationality = new JTextField(employeeBasicInformationModel.get().getNationality());
        nationality.setBounds(450, 250, 200, 30);
        nationality.setFont(Constant.newFONTBold());
        JLabel jLabelNationality = new JLabel("国籍信息：");
        jLabelNationality.setBounds(250, 250, 150, 30);
        jLabelNationality.setFont(Constant.newFontPlain());

        // 婚姻状况输入框
        marriageStatus = new JTextField(employeeBasicInformationModel.get().getMarriageStatus());
        marriageStatus.setBounds(450, 300, 200, 30);
        marriageStatus.setFont(Constant.newFONTBold());
        JLabel jLabelMarriage = new JLabel("婚姻状况：");
        jLabelMarriage.setBounds(250, 300, 150, 30);
        jLabelMarriage.setFont(Constant.newFontPlain());

        JButton buttonFinish = new JButton("保存并提交");
        buttonFinish.setBounds(100, 450, 170, 40);
        buttonFinish.setFont(Constant.newFONTBold());
        buttonFinish.setBackground(Constant.COLOR3);
        buttonFinish.setForeground(Constant.COLOR1);

        JButton buttonUser = new JButton("基本信息");
        buttonUser.setBounds(250, 450, 170, 40);
        buttonUser.setFont(Constant.newFONTBold());
        buttonUser.setBackground(Constant.COLOR3);
        buttonUser.setForeground(Constant.COLOR1);

        JButton buttonEducation = new JButton("学历信息");
        buttonEducation.setBounds(400, 450, 170, 40);
        buttonEducation.setFont(Constant.newFONTBold());
        buttonEducation.setBackground(Constant.COLOR3);
        buttonEducation.setForeground(Constant.COLOR1);

        JButton buttonJob = new JButton("工作信息");
        buttonJob.setBounds(550, 450, 170, 40);
        buttonJob.setFont(Constant.newFONTBold());
        buttonJob.setBackground(Constant.COLOR3);
        buttonJob.setForeground(Constant.COLOR1);

        //完成按钮功能实现
        Optional<EmployeeBasicInformationModel> finalEmployeeBasicInformationModel = employeeBasicInformationModel;
        buttonFinish.addActionListener(event -> {
            if (age.getText() == null || age.getText().isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(), "请完善信息", "警告", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!Util.checkDate(birthday)) {
                return;
            }
            if (sex.getText().length() > 2) {
                JOptionPane.showMessageDialog(new JFrame(), "性别信息错误", "警告", JOptionPane.ERROR_MESSAGE);
                return;
            }
            EmployeeBasicInformationModel e = EmployeeBasicInformationModel.builder()
                    .birthday(Util.emptyToString(birthday))
                    .nationality(nationality.getText())
                    .age(Integer.parseInt(age.getText()))
                    .marriageStatus(marriageStatus.getText())
                    .sex(sex.getText())
                    .uuid(finalEmployeeBasicInformationModel.get().getUuid())
                    .name(name.getText())
                    .build();
            if (Integer.valueOf(1).equals(employeeBasicInformationService.updateOrInsert(e))) {
                JOptionPane.showMessageDialog(new JFrame(), "更新成功", "信息", JOptionPane.PLAIN_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "更新失败", "错误", JOptionPane.ERROR_MESSAGE);
            }
        });
        //学历信息按钮功能实现
        buttonEducation.addActionListener(event -> {
            this.dispose();
            new EducationInformationFrame(uuid);
        });
        //工作信息按钮功能实现
        buttonJob.addActionListener(event -> {
            this.dispose();
            new JobInformationFrame(uuid);
        });
        //基本信息按钮功能实现
        buttonUser.addActionListener(event -> {

        });
        //所有组件用容器装载
        container.add(jLabelUUID);
        container.add(jLabelName);
        container.add(jLabelSex);
        container.add(jLabelAge);
        container.add(jLabelBirthday);
        container.add(jLabelNationality);
        container.add(jLabelMarriage);
        container.add(name);
        container.add(sex);
        container.add(age);
        container.add(birthday);
        container.add(nationality);
        container.add(marriageStatus);
        container.add(buttonFinish);
        container.add(buttonUser);
        container.add(buttonJob);
        container.add(buttonEducation);
        this.setTitle("");
        this.setTitle("个人信息"); //设置标题
        this.setLayout(null);// 设置布局方式为绝对定位
        this.setBounds(0, 0, 850, 550);
        this.setResizable(false);// 窗体大小不能改变
        this.setLocationRelativeTo(null);// 居中显示
        this.setVisible(true);// 窗体可见
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    static synchronized UserInformationFrame of(String uuid) {
        if (singleton == null) {
            singleton = new UserInformationFrame(uuid);
        }
        return singleton;
    }

    public void setVisible() {
        super.setVisible(true);
    }
}
