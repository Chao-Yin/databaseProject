package gui;

import constant.Constant;
import model.DepartmentModel;
import model.EmployeeJobInformationModel;
import service.DepartmentService;
import service.JobInformationService;
import util.Util;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class JobInformationFrame extends JFrame {
    //工作信息表：
    private JTextField job;
    private JTextField job_StartTime;
    private JTextField job_EndTime;
    private JTextField status;
    private JobInformationService jobInformationService = new JobInformationService();
    private DepartmentService departmentService = new DepartmentService();

    JobInformationFrame(String uuid) {
        Optional<EmployeeJobInformationModel> employeeJobInformationModel =
                jobInformationService.selectByPrimaryKey(uuid);
        if (!employeeJobInformationModel.isPresent()) {
            employeeJobInformationModel = Optional.of(new EmployeeJobInformationModel());
            employeeJobInformationModel.get().setUuid(uuid);
        }
        // 创建一个容器
        Container container = this.getContentPane();
        JLabel jLabelUUID = new JLabel("UUID:" + employeeJobInformationModel.get().getUuid());
        jLabelUUID.setBounds(350, 5, 200, 50);
        jLabelUUID.setFont(Constant.newFONTBold());

        int jobId = employeeJobInformationModel.get().getJobId();
        String departmentName;
        if (departmentService.departmentName(jobId).isPresent()) {
            departmentName = departmentService.departmentName(jobId).get();
        } else {
            departmentName = "";
        }
        job = new JTextField(departmentName);
        job.setBounds(450, 50, 200, 30);
        job.setFont(Constant.newFONTBold());
        job.setText(departmentName);
        JLabel jLabelJob = new JLabel("工作岗位");
        jLabelJob.setBounds(250, 50, 150, 30);
        jLabelJob.setFont(Constant.newFontPlain());

        status = new JTextField(employeeJobInformationModel.get().getJobStatus());
        status.setBounds(450, 100, 200, 30);
        status.setFont(Constant.newFONTBold());
        status.setText(employeeJobInformationModel.get().getJobStatus());
        JLabel jLabelStatus = new JLabel("是否在岗");
        jLabelStatus.setBounds(250, 100, 150, 30);
        jLabelStatus.setFont(Constant.newFontPlain());

        job_StartTime = new JTextField(employeeJobInformationModel.get().getStartTime());
        job_StartTime.setBounds(450, 150, 200, 30);
        job_StartTime.setFont(Constant.newFONTBold());
        job_StartTime.setText(employeeJobInformationModel.get().getStartTime());
        JLabel jLabelJobStartTime = new JLabel("工作岗位开始时间");
        jLabelJobStartTime.setBounds(250, 150, 400, 30);
        jLabelJobStartTime.setFont(Constant.newFontPlain());

        job_EndTime = new JTextField(employeeJobInformationModel.get().getEndTime());
        job_EndTime.setBounds(450, 200, 200, 30);
        job_EndTime.setFont(Constant.newFONTBold());
        job_EndTime.setText(employeeJobInformationModel.get().getEndTime());
        JLabel jLabelJobEndTime = new JLabel("工作岗位结束时间");
        jLabelJobEndTime.setBounds(250, 200, 500, 30);
        jLabelJobEndTime.setFont(Constant.newFontPlain());

        JButton buttonFinish = new JButton("保存并提交");
        buttonFinish.setBounds(100, 450, 170, 40);
        buttonFinish.setFont(Constant.newFONTBold());
        buttonFinish.setBackground(Constant.COLOR3);
        buttonFinish.setForeground(Constant.COLOR1);

        JButton buttonUser = new JButton("基本信息");
        buttonUser.setBounds(250, 450, 170, 40);
        buttonUser.setFont(Constant.newFONTBold());
        buttonUser.setBackground(Constant.COLOR3);
        buttonUser.setForeground(Constant.COLOR1);

        JButton buttonEducation = new JButton("学历信息");
        buttonEducation.setBounds(400, 450, 170, 40);
        buttonEducation.setFont(Constant.newFONTBold());
        buttonEducation.setBackground(Constant.COLOR3);
        buttonEducation.setForeground(Constant.COLOR1);

        JButton buttonJob = new JButton("工作信息");
        buttonJob.setBounds(550, 450, 170, 40);
        buttonJob.setFont(Constant.newFONTBold());
        buttonJob.setBackground(Constant.COLOR3);
        buttonJob.setForeground(Constant.COLOR1);

        //完成按钮功能实现
        Optional<EmployeeJobInformationModel> finalEmployeeJobInformationModel = employeeJobInformationModel;
        buttonFinish.addActionListener(event -> {
            if (job.getText() == null || job.getText().isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(), "请完善信息", "警告", JOptionPane.ERROR_MESSAGE);
                return;
            }
            DepartmentModel d = DepartmentModel.builder()
                    .job(job.getText()).build();
            if (!Util.checkDate(job_StartTime) || !Util.checkDate(job_EndTime)) {
                return;
            }
            int jobId2 = departmentService.jobId(job.getText()).get();
            System.out.println(jobId2);
            EmployeeJobInformationModel e = EmployeeJobInformationModel.builder()
                    .id(finalEmployeeJobInformationModel.get().getId())
                    .jobId(jobId2 == -1 ? 0 : jobId2)
                    .jobStatus(status.getText())
                    .startTime(Util.emptyToString(job_StartTime))
                    .endTime(Util.emptyToString(job_EndTime))
                    .uuid(finalEmployeeJobInformationModel.get().getUuid())
                    .build();
            if (Integer.valueOf(1).equals(jobInformationService.updateOrInsert(e))) {
                JOptionPane.showMessageDialog(new JFrame(), "更新成功", "信息", JOptionPane.PLAIN_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "更新失败", "错误", JOptionPane.ERROR_MESSAGE);
            }
        });
        //学历信息按钮功能实现
        buttonEducation.addActionListener(event -> {
            this.dispose();
            new EducationInformationFrame(uuid);
        });
        //工作信息按钮功能实现
        buttonJob.addActionListener(event -> {

        });
        //基本信息按钮功能实现
        buttonUser.addActionListener(event -> {
            this.dispose();
            new UserInformationFrame(uuid);

        });
        container.add(jLabelUUID);
        container.add(jLabelJob);
        container.add(jLabelJobStartTime);
        container.add(jLabelJobEndTime);
        container.add(jLabelStatus);
        container.add(job);
        container.add(job_StartTime);
        container.add(job_EndTime);
        container.add(status);
        container.add(buttonFinish);
        container.add(buttonUser);
        container.add(buttonJob);
        container.add(buttonEducation);
        this.setTitle("");
        //this.setTitle("个人信息"); //设置标题
        this.setLayout(null);// 设置布局方式为绝对定位
        this.setBounds(0, 0, 850, 550);
        this.setResizable(false);// 窗体大小不能改变
        this.setLocationRelativeTo(null);// 居中显示
        this.setVisible(true);// 窗体可见
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }
}
