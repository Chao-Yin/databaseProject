package gui;


import constant.Constant;
import model.UserInformationModel;
import service.UserInformationService;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Optional;

public class LoginFrame extends JFrame {
    private static final long serialVersionUID = -6256528270698337060L;
    private JTextField uuid; // 用户名
    private JPasswordField password; // 密码
    private JLabel labelUUID;
    private JLabel labelPassword;
    private JButton buttonLogin; // 按钮
    private JButton buttonRegister;
    private int wx, wy;
    private boolean isDragging = false;
    private JPanel contentPanel;
    private UserInformationService userInformationService = new UserInformationService();

    public LoginFrame() {

        // 设置无标题栏
//        setUndecorated(true);
        // 监听鼠标 确保窗体能够拖拽
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                isDragging = true;
                wx = e.getX();
                wy = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                isDragging = false;
            }
        });

        /**
         * 设置拖拽
         */
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                if (isDragging) {
                    int left = getLocation().x;
                    int top = getLocation().y;
                    setLocation(left + e.getX() - wx, top + e.getY() - wy);
                }
            }
        });

        setBounds(300, 200, 450, 400);
        setLocationRelativeTo(null);
        contentPanel = new JPanel();
        contentPanel.setBackground(Color.GRAY);
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setResizable(false);
        setContentPane(contentPanel);
        contentPanel.setLayout(null);

        buttonLogin = new JButton("登录");
        buttonLogin.setBounds(225, 285, 170, 40);
        buttonLogin.setBackground(Constant.COLOR6);
        buttonLogin.setForeground(Color.WHITE);
        buttonLogin.setBorder(null);
        buttonLogin.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 25));
        buttonLogin.setPreferredSize(new Dimension(170, 40));
        buttonLogin.setFocusPainted(false);
        contentPanel.add(buttonLogin);

        buttonLogin.addActionListener(event -> {
            Optional<UserInformationModel> userInformationModel = userInformationService.selectByPrimaryKey(uuid.getText());
            if (userInformationModel.isPresent()) {
                dispose();
                new SystemWindow(userInformationModel.get().getUuid());
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "用户名与密码不匹配！", "错误", JOptionPane.ERROR_MESSAGE);
            }
        });

        buttonRegister = new JButton("注册");
        buttonRegister.setBounds(37, 285, 170, 40);
        buttonRegister.setFocusPainted(false);
        buttonRegister.setBackground(Constant.COLOR7);
        buttonRegister.setForeground(Color.WHITE);
        buttonRegister.setBorder(null);
        buttonRegister.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 25));
        buttonRegister.setPreferredSize(new Dimension(170, 40));
        contentPanel.add(buttonRegister);

        buttonRegister.addActionListener(e -> new RegisterFrame());

        // 用户号码登录输入框
        uuid = new JTextField("123");
        uuid.setBounds(170, 167, 219, 35);
        uuid.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 25));
        uuid.setBorder(null);
        contentPanel.add(uuid);
        // 登录输入框旁边的文字
        labelUUID = new JLabel("工号");
        labelUUID.setBounds(37, 170, 126, 27);
        labelUUID.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 25));
        labelUUID.setForeground(Color.WHITE);
        contentPanel.add(labelUUID);

        // 密码输入框
        password = new JPasswordField("123");
        password.setBounds(170, 212, 219, 35);
        password.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 30));
        password.setBorder(null);
        contentPanel.add(password);
        labelPassword = new JLabel("密码");
        labelPassword.setBounds(37, 215, 126, 27);
        labelPassword.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 25));
        labelPassword.setForeground(Color.WHITE);
        contentPanel.add(labelPassword);

        JLabel jLabelTitle = new JLabel("人事管理系统");
        jLabelTitle.setForeground(Color.WHITE);
        jLabelTitle.setBackground(Color.WHITE);
//        jLabelTitle.setFont(new Font("Microsoft JhengHei Light", Font.PLAIN, 58));
        jLabelTitle.setFont(new Font("宋体", Font.PLAIN, 58));
        jLabelTitle.setBounds(37, 60, 357, 80);
        contentPanel.add(jLabelTitle);
    }

    public static void main(String[] args) {
        LoginFrame loginFrame = new LoginFrame();
        loginFrame.setVisible(true);
    }
}