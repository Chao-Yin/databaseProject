package gui;

import constant.Constant;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import model.OverAllModel;
import service.OverAllInformationService;
import service.UserInformationService;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

@Slf4j(topic = "SystemWindow")
public class SystemWindow extends JFrame {
    JTable jTable = new JTable();
    // 变量声明
    private JButton buttonUpdate;
    private JButton uuidQuery;
    private JButton marriageQuery;
    private JButton educationQuery;
    private JButton jobQuery;
    private JButton logOut;
    private JButton delete;
    private JToolBar modifyEmployee;
    private JToolBar queryEmployee;
    private int wx, wy;
    private boolean isDragging = false;
    private UserInformationService userInformationService = new UserInformationService();
    private OverAllInformationService overAllInformationService = new OverAllInformationService();

    public SystemWindow(String uuid) {
        if (uuid == null || uuid.isEmpty() || !userInformationService.selectByPrimaryKey(uuid).isPresent()) {
            JOptionPane.showMessageDialog(new JFrame(), "工号错误");
            return;
        }
        // 监听鼠标 确保窗体能够拖拽
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                isDragging = true;
                wx = e.getX();
                wy = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                isDragging = false;
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (isDragging) {
                    int left = getLocation().x;
                    int top = getLocation().y;
                    setLocation(left + e.getX() - wx, top + e.getY() - wy);
                }
            }
        });

        //设置容器
        JPanel jPanel = new JPanel(new BorderLayout());
        //设置工具栏
        modifyEmployee = new JToolBar("操作员工信息");
        queryEmployee = new JToolBar("查询员工信息");
        //创建按钮
        buttonUpdate = new JButton("修改员工信息");
        buttonUpdate.setBounds(37, 285, 170, 40);
        delete = new JButton("删除员工");
        uuidQuery = new JButton("按工号查询");
        uuidQuery.setBounds(67, 285, 200, 40);
        marriageQuery = new JButton("按婚姻状况查询");
        marriageQuery.setBounds(97, 285, 230, 40);
        educationQuery = new JButton("按学历查询");
        educationQuery.setBounds(127, 285, 260, 40);
        jobQuery = new JButton("按工作岗位查询");
        jobQuery.setBounds(157, 285, 290, 40);
        logOut = new JButton("退出登录");
        logOut.setBounds(187, 285, 320, 40);
        buttonUpdate.setBackground(Color.lightGray);

        uuidQuery.setBackground(Color.lightGray);
        marriageQuery.setBackground(Color.lightGray);
        educationQuery.setBackground(Color.lightGray);
        jobQuery.setBackground(Color.lightGray);
        logOut.setBackground(Color.lightGray);
        delete.setBackground(Color.LIGHT_GRAY);


        buttonUpdate.addActionListener(event -> {
            UserInformationFrame.of(uuid).setVisible();
        });

        logOut.addActionListener(event -> {
            dispose();
            LoginFrame.main(new String[0]);
        });

        delete.addActionListener(event -> {
            int i = JOptionPane.showConfirmDialog(null, "是否确认删除");
            if (i == 0) {
                if (Integer.valueOf(1).equals(userInformationService.deleteByPrimaryKey(uuid))) {
                    JOptionPane.showMessageDialog(null, "删除成功");
                    dispose();
                    LoginFrame.main(null);
                } else {
                    JOptionPane.showMessageDialog(null, "删除失败");
                }
            }

        });

        uuidQuery.addActionListener(event -> {
            showQueryStatistics(overAllInformationService.selectAllNewestJobStatusAndHighestEducation());
            log.info("查询完成");
        });

        marriageQuery.addActionListener(event -> {
            Object[] sports = {"已婚","未婚","离异","保持神秘"};

            String marriageStatus=String.valueOf(JOptionPane.showInputDialog(null,"请问你的婚姻状态是","婚姻信息",JOptionPane.QUESTION_MESSAGE,
                    null,sports,sports[3]));

            //String marrigeStatus=JOptionPane.showOptionDialog(null, "请问你的婚姻状况是", "婚姻状态"
                    //,JOptionPane.YES_NO_CANCEL_OPTION
                    //,JOptionPane.QUESTION_MESSAGE,null, sports, sports[0]);
            showQueryStatistics(overAllInformationService.selectAllNewestJobStatusAndHighestEducationByMarriage(marriageStatus));
            log.info("查询完成");
//            repaint();
        });

        educationQuery.addActionListener(event -> {
            //String education = JOptionPane.showInputDialog(null, "请输入学历信息");
            Object[] sports = {"小学","初中","高中","大学","硕士","学士","博士","关系富"};

            String education=String.valueOf(JOptionPane.showInputDialog(null,"请问你的学历是","学历信息",JOptionPane.QUESTION_MESSAGE,
                    null,sports,sports[3]));
            showQueryStatistics(overAllInformationService.selectAllNewestJobStatusAndHighestEducationByEducation(education));
        });

        jobQuery.addActionListener(event -> {
            //String job = JOptionPane.showInputDialog(null, "请输入工作岗位");
            Object[] sports = {"岗位一","岗位二","岗位三","岗位四","程序猿","女程序猿"};

            String job=String.valueOf(JOptionPane.showInputDialog(null,"请问你的工作岗位是","工作信息",JOptionPane.QUESTION_MESSAGE,
                    null,sports,sports[3]));
            showQueryStatistics(overAllInformationService.selectAllNewestJobStatusAndHighestEducationByJob(job));
        });
        //把按钮添加到工具栏
        modifyEmployee.add(buttonUpdate);
        modifyEmployee.addSeparator();
        modifyEmployee.add(logOut);
        modifyEmployee.addSeparator();
        modifyEmployee.add(delete);
        queryEmployee.add(uuidQuery);
        queryEmployee.add(marriageQuery);
        queryEmployee.add(educationQuery);
        queryEmployee.add(jobQuery);
        //设置工具栏可以拖动
        modifyEmployee.setFloatable(false);
        queryEmployee.setFloatable(false);
        //把工具栏添加到顶部和底部
        jPanel.add(modifyEmployee, BorderLayout.PAGE_END);
        jPanel.add(queryEmployee, BorderLayout.PAGE_START);
        // 添加表单信息
        this.setLocationRelativeTo(null);
        setBounds(300, 100, 900, 600);
        this.setContentPane(jPanel);
        this.setVisible(true);
    }

    @SneakyThrows
    private void showQueryStatistics(List<OverAllModel> overAllModelList) {
        DefaultTableModel basicInformationTableModel;
        if (jTable != null) {
            basicInformationTableModel = (DefaultTableModel) jTable.getModel();
            basicInformationTableModel.fireTableDataChanged();
        }
        if (overAllModelList == null || overAllModelList.size() == 0) {
            JOptionPane.showMessageDialog(null, "没有信息");
            return;
        }
        Object[][] data = new Object[overAllModelList.size()][Constant.HEADERS.length];
        OverAllModel overAllModel;
        for (int i = 0; i < data.length; i++) {
            overAllModel = overAllModelList.get(i);
            System.out.println(overAllModel);
            data[i][0] = overAllModel.getUuid();
            data[i][1] = overAllModel.getName();
            data[i][2] = overAllModel.getSex();
            data[i][3] = overAllModel.getAge();
            data[i][4] = overAllModel.getBirthday();
            data[i][5] = overAllModel.getNationality();
            data[i][6] = overAllModel.getMarriageStatus();
            data[i][7] = overAllModel.getEducationName();
            data[i][8] = overAllModel.getEducationMajor();
            data[i][9] = overAllModel.getEducationGetYear();
            data[i][10] = overAllModel.getEducationInstitution();
            data[i][11] = overAllModel.getForeignLanguage();
            data[i][12] = overAllModel.getJob();
            data[i][13] = overAllModel.getDepartmentName();
            data[i][14] = overAllModel.getJobStatus();
            data[i][15] = overAllModel.getEducationGetYear();
        }

        basicInformationTableModel = new DefaultTableModel(data, Constant.HEADERS) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }

//            @Override
//            public void fireTableStructureChanged() {
//                super.fireTableStructureChanged();
//                System.out.println("destroy table");
//            }
        };

        basicInformationTableModel.isCellEditable(0, 0);
        jTable.setModel(basicInformationTableModel);
        // 非自适应,否则没有表头
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.setVisible(true);
        jTable.repaint();
        add(jScrollPane);
        add(jTable, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
