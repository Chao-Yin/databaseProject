package gui;

import constant.Constant;
import model.EducationInformationModel;
import service.EducationInformationService;
import util.Util;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class EducationInformationFrame extends JFrame {
    //学历信息表
    private JTextField education;
    private JTextField graduate_Time;
    private JTextField colleage;
    private JTextField proffession;
    private JTextField englsih_Level;
    private EducationInformationService educationInformationService = new EducationInformationService();

    EducationInformationFrame(String uuid) {
        EducationInformationModel educationInformationModel1 =
                educationInformationService.selectByPrimaryKey(uuid);
        Optional<EducationInformationModel> educationInformationModel = Optional.of(educationInformationModel1);
        if (educationInformationModel1 == null) {
            educationInformationModel = Optional.of(new EducationInformationModel());
            educationInformationModel.get().setUserId(uuid);
            //id=uuid;
        }
        // 创建一个容器
        Container container = this.getContentPane();
        JLabel jLabelUUID = new JLabel("UUID:" + educationInformationModel.get().getUserId());
        jLabelUUID.setBounds(350, 5, 200, 50);
        jLabelUUID.setFont(Constant.newFONTBold());

        education = new JTextField(educationInformationModel.get().getEducationName());
        education.setBounds(450, 50, 200, 30);
        education.setFont(Constant.newFONTBold());
        education.setText(educationInformationModel.get().getEducationName());
        JLabel jLabelEducation = new JLabel("学历");
        jLabelEducation.setBounds(250, 50, 50, 30);
        jLabelEducation.setFont(Constant.newFontPlain());

        graduate_Time = new JTextField(educationInformationModel.get().getEducationGetYear());
        graduate_Time.setBounds(450, 100, 200, 30);
        graduate_Time.setFont(Constant.newFONTBold());
        graduate_Time.setText(educationInformationModel.get().getEducationGetYear());
        JLabel jLabelGraduate_Time = new JLabel("毕业时间");
        jLabelGraduate_Time.setBounds(250, 100, 150, 30);
        jLabelGraduate_Time.setFont(Constant.newFontPlain());

        colleage = new JTextField(educationInformationModel.get().getEducationInstitution());
        colleage.setBounds(450, 150, 200, 30);
        colleage.setFont(Constant.newFONTBold());
        colleage.setText(educationInformationModel.get().getEducationInstitution());
        JLabel jLabelCollage = new JLabel("学校");
        jLabelCollage.setBounds(250, 150, 50, 30);
        jLabelCollage.setFont(Constant.newFontPlain());

        proffession = new JTextField(educationInformationModel.get().getEducationMajor());
        proffession.setBounds(450, 200, 200, 30);
        proffession.setFont(Constant.newFONTBold());
        proffession.setText(educationInformationModel.get().getEducationMajor());
        JLabel jLabelProfession = new JLabel("专业");
        jLabelProfession.setBounds(250, 200, 50, 30);
        jLabelProfession.setFont(Constant.newFontPlain());

        englsih_Level = new JTextField(educationInformationModel.get().getForeignLanguage());
        englsih_Level.setBounds(450, 250, 200, 30);
        englsih_Level.setFont(Constant.newFONTBold());
        JLabel jLabelEnglish_Level = new JLabel("英语等级");
        jLabelEnglish_Level.setBounds(250, 250, 150, 30);
        jLabelEnglish_Level.setFont(Constant.newFontPlain());

        JButton buttonFinish = new JButton("保存并提交");
        buttonFinish.setBounds(100, 450, 170, 40);
        buttonFinish.setFont(Constant.newFONTBold());
        buttonFinish.setBackground(Constant.COLOR3);
        buttonFinish.setForeground(Constant.COLOR1);

        JButton buttonUser = new JButton("基本信息");
        buttonUser.setBounds(250, 450, 170, 40);
        buttonUser.setFont(Constant.newFONTBold());
        buttonUser.setBackground(Constant.COLOR3);
        buttonUser.setForeground(Constant.COLOR1);

        JButton buttonEducation = new JButton("学历信息");
        buttonEducation.setBounds(400, 450, 170, 40);
        buttonEducation.setFont(Constant.newFONTBold());
        buttonEducation.setBackground(Constant.COLOR3);
        buttonEducation.setForeground(Constant.COLOR1);

        JButton buttonJob = new JButton("工作信息");
        buttonJob.setBounds(550, 450, 170, 40);
        buttonJob.setFont(Constant.newFONTBold());
        buttonJob.setBackground(Constant.COLOR3);
        buttonJob.setForeground(Constant.COLOR1);



        //完成按钮功能实现
        Optional<EducationInformationModel> finalEducationInformationModel = educationInformationModel;
        buttonFinish.addActionListener(event -> {
            if (colleage.getText() == null || colleage.getText().isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(), "请完善信息", "警告", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!Util.checkDate(graduate_Time)) {
                return;
            }
            EducationInformationModel e = EducationInformationModel.builder()
                    .id(finalEducationInformationModel.get().getId())
                    .educationName(education.getText())
                    .educationMajor(proffession.getText())
                    .educationGetYear(graduate_Time.getText())
                    .educationInstitution(colleage.getText())
                    .foreignLanguage(englsih_Level.getText())
                    .userId(finalEducationInformationModel.get().getUserId())
                    .build();
            if (Integer.valueOf(1).equals(educationInformationService.updateOrInsert(e))) {
                JOptionPane.showMessageDialog(new JFrame(), "更新成功", "信息", JOptionPane.PLAIN_MESSAGE);
                dispose();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "更新失败", "错误", JOptionPane.ERROR_MESSAGE);
            }
        });
        //学历信息按钮功能实现
        buttonEducation.addActionListener(event -> {

        });
        //工作信息按钮功能实现
        buttonJob.addActionListener(event -> {
            this.dispose();
            new JobInformationFrame(uuid);
        });
        //基本信息按钮功能实现
        buttonUser.addActionListener(event -> {
            this.dispose();
            new UserInformationFrame(uuid);

        });
        container.add(jLabelUUID);
        container.add(jLabelCollage);
        container.add(jLabelEducation);
        container.add(jLabelEnglish_Level);
        container.add(jLabelGraduate_Time);
        container.add(jLabelProfession);
        container.add(education);
        container.add(englsih_Level);
        container.add(proffession);
        container.add(graduate_Time);
        container.add(colleage);
        container.add(buttonFinish);
        container.add(buttonUser);
        container.add(buttonJob);
        container.add(buttonEducation);
        this.setTitle("");
        //this.setTitle("个人信息"); //设置标题
        this.setLayout(null);// 设置布局方式为绝对定位
        this.setBounds(0, 0, 850, 550);
        this.setResizable(false);// 窗体大小不能改变
        this.setLocationRelativeTo(null);// 居中显示
        this.setVisible(true);// 窗体可见
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }
}
