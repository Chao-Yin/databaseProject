package gui;

import constant.Constant;
import model.UserInformationModel;
import org.skife.jdbi.v2.sqlobject.Transaction;
import service.EducationInformationService;
import service.EmployeeBasicInformationService;
import service.JobInformationService;
import service.UserInformationService;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

public class RegisterFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JTextField uuid; // 工号
    private JPasswordField password; // 密码
    private JPasswordField repeatPassword; // 重复密码
    private JLabel labelUser;
    private JLabel labelPwd;
    private JLabel labelPassword;
    private JButton buttonRegister;

    private UserInformationService userInformationService = new UserInformationService();
    private EducationInformationService educationInformationService = new EducationInformationService();
    private JobInformationService jobInformationService = new JobInformationService();
    private EmployeeBasicInformationService employeeBasicInformationService = new EmployeeBasicInformationService();

    public RegisterFrame() {
        // 创建一个容器
        Container con = this.getContentPane();
        con.setBackground(Color.gray);

        // 用户号码登录输入框
        uuid = new JTextField();
        uuid.setBounds(200, 50, 250, 40);
        uuid.setFont(new Font("宋体", Font.BOLD, 20));
        // 登录输入框旁边的文字
        labelUser = new JLabel("填写五位数工号");
        labelUser.setBounds(40, 50, 150, 40);
        labelUser.setFont(Constant.newFONTBold());
        labelUser.setBackground(Color.white);
        labelUser.setForeground(Color.WHITE);

        // 密码输入框
        password = new JPasswordField();
        password.setBounds(200, 100, 250, 40);
        password.setFont(Constant.newFONTBold());
        repeatPassword = new JPasswordField();
        repeatPassword.setBounds(200, 150, 250, 40);
        repeatPassword.setFont(Constant.newFONTBold());
        // 密码输入框旁边的文字
        labelPwd = new JLabel("填写密码");
        labelPwd.setBounds(100, 100, 100, 40);
        labelPwd.setFont(Constant.newFONTBold());
        labelPassword = new JLabel("重填密码");
        labelPassword.setBounds(100, 150, 100, 40);
        labelPassword.setFont(Constant.newFONTBold());
        labelPwd.setBackground(Color.white);
        labelPwd.setForeground(Color.WHITE);
        labelPassword.setBackground(Color.white);
        labelPassword.setForeground(Color.WHITE);

        // 注册按钮
        buttonRegister = new JButton("注册");
        buttonRegister.setBounds(200, 220, 150, 50);
        buttonRegister.setFont(Constant.newFONTBold());
        buttonRegister.setBackground(Constant.COLOR1);
        buttonRegister.setForeground(Constant.COLOR3);
        buttonRegister.addActionListener(e -> {
            String inputUUId = uuid.getText();
            if (inputUUId.length() < 5) {
                JOptionPane.showMessageDialog(new JFrame(), "工号位数小于5", "错误",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                registerUser(inputUUId, String.valueOf(password.getPassword()), String.valueOf(repeatPassword.getPassword()));
            }
        });
        // 所有组件用容器装载
        con.add(labelUser);
        con.add(labelPwd);
        con.add(labelPassword);
        con.add(uuid);
        con.add(password);
        con.add(repeatPassword);
        con.add(buttonRegister);
        this.setTitle("注册窗口");// 设置窗口标题
        this.setBackground(Color.gray);
        this.setLayout(null);// 设置布局方式为绝对定位
        this.setBounds(0, 0, 550, 350);
        this.setResizable(false);// 窗体大小不能改变
        this.setLocationRelativeTo(null);// 居中显示
        this.setVisible(true);// 窗体可见
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    @Transaction
    private void registerUser(String uuid, String password, String repeatPassword) {
        if (!password.equals(repeatPassword)) {
            JOptionPane.showMessageDialog(new JFrame(), "两次输入密码不相同！", "错误",
                    JOptionPane.ERROR_MESSAGE);
            this.password.setText("");
            this.repeatPassword.setText("");
            return;
        }
        Optional<UserInformationModel> userInformationModel = userInformationService.selectByPrimaryKey(uuid);
        if (userInformationModel.isPresent()) {
            JOptionPane.showMessageDialog(new JFrame(), "注册失败,已有相同的工号,换个工号试试吧！", "错误", JOptionPane.ERROR_MESSAGE);
            return;
        }
        userInformationService.insert(uuid, password);
        educationInformationService.create(uuid);
        jobInformationService.create(uuid);
        employeeBasicInformationService.create(uuid);
        JOptionPane.showMessageDialog(new JFrame(),
                "注册成功！", "恭喜", JOptionPane.PLAIN_MESSAGE);
        dispose();
    }
}
