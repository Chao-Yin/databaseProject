package util;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.time.LocalDate;

@Slf4j(topic = "Util")
public class Util {
    public static boolean checkDate(JTextField text) {
        if (text.getText() == null || text.getText().isEmpty()) {
            return true;
        }
        try {
            LocalDate.parse(text.getText());
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), "日期格式不正确,样例:2000-01-01", "警告", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public static String emptyToString(JTextField text) {
        String str = text.getText();
        if (str == null || str.isEmpty()) {
            log.info("string 为空,将日期设置为null");
            return null;
        }
        return str;
    }
}
