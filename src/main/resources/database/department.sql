create table department
(
    id              int(11)      not null comment '部门编号'
        primary key auto_increment,
    department_name varchar(20)  not null comment '部门信息',
    job             varchar(45)  not null comment '工作岗位',
    extra_key1      varchar(255) null comment '预留字段1',
    extra_key2      varchar(255) null comment '预留字段2',
    extra_key3      int          null comment '预留字段3'
)
    charset = utf8;

